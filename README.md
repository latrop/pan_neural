# pan_neural

A tool for finding objects of a specified type in ht whole Pan-STARRS survey using CNN algorithm.

# Requirements

Python 3.6 (or newer) with following modules

	- requests
	- numpy
	- scipy
	- astropy
	- photutils
	- keras
	- tensorflow

# Usage

    main.py --node-idx NODE_IDX --num-of-nodes NUM_OF_NODES

arguments:

    --num-of-nodes NUM_OF_NODES    Total number of nodes (i.e. the number of machines that are
	                               used do scatter the whole survey). Each node will get 1/n-th
								   part of the whole job.
    --node-idx NODE_IDX            Current node unique index (from 0 to num-of-nodes - 1)
