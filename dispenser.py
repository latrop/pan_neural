#!/usr/bin/env python3

import sys
import os
from os import path
import datetime


def main():
    common_todo_path = "todo.dat"
    # If theres no todo file, create one and populate it with all the tiles
    if not path.exists(common_todo_path):
        common_todo = open(common_todo_path, "w")
        cell_start = 636
        cell_end = 2643
        for cell_number in range(cell_start, cell_end+1):
            for sky_cell_location in range(0, 100):
                common_todo.write("%i  %i\n" % (cell_number, sky_cell_location))
        common_todo.close()

    # Load tiles
    tiles = open(common_todo_path).readlines()
    tiles.reverse()
    if len(tiles) == 0:
        print("Empty")
        return

    # Pop first 100 tiles
    for i in range(min(len(tiles), 100)):
        print(tiles.pop(), end="")
    # Write the rest of the tiles back to the file
    tiles.reverse()
    common_todo = open(common_todo_path, "w")
    for line in tiles:
        common_todo.write(line)
    common_todo.close()

    # Log the connection
    log = open("dispenser.log", "a")
    t_string = datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S")
    log.write("%10s at %s\n" % (sys.argv[1], t_string))


if __name__ == '__main__':
    try:
        os.open("lock", os.O_CREAT|os.O_EXCL)
    except FileExistsError:
        print("Busy")
        exit()
    main()
    os.remove("lock")
