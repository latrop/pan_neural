#! /usr/bin/env python

from os import path
import warnings
import numpy as np
from scipy.ndimage import zoom, gaussian_filter
from astropy.io import fits
from astropy.wcs import WCS
from astropy.wcs import FITSFixedWarning
from photutils import detect_threshold
from photutils import detect_sources
from photutils import source_properties
from astropy.utils.exceptions import AstropyDeprecationWarning
warnings.simplefilter('ignore', AstropyDeprecationWarning)
warnings.simplefilter('ignore', FITSFixedWarning)
warnings.filterwarnings('ignore', module="astropy.stats.sigma_clipping")


class MultibandField(object):
    def __init__(self, hdu_list, band_list):
        self.band_list = band_list
        # Load all data
        self.all_data = {}
        self.all_headers = {}
        self.all_wcs = {}
        for band, hdu in zip(band_list, hdu_list):
            self.all_data[band] = hdu[1].data.copy()
            self.all_headers[band] = hdu[1].header.copy()
            self.all_wcs[band] = WCS(hdu[1].header.copy())
        self.y_image_size, self.x_image_size = self.all_data[band_list[0]].shape
        self.combined = np.mean([d for d in self.all_data.values()], axis=0)
        self.combined = gaussian_filter(self.combined, sigma=2)

    def crop_out_objects(self, ref_band="r", minsize=24):
        # Detect sources on the field
        threshold = detect_threshold(self.combined, nsigma=3.5)
        segm = detect_sources(self.combined, threshold, npixels=15)
        if segm is None:
            return None
        cat = source_properties(self.all_data[ref_band], segm)

        # Crop out images
        cropped_objects = []
        for idx, obj in enumerate(cat):
            object_size = max((obj.bbox_xmax.value-obj.bbox_xmin.value),
                              (obj.bbox_ymax.value-obj.bbox_ymin.value)) / 1.4
            # Check if the object size is too small
            if object_size < minsize:
                # If so, we skip the object
                continue
            # Check the object flux in all passbands. If the flux differ too much between
            # the bassbands, it means that the object is an artifact thatt present
            # only in one passband
            obj_segm = np.where(segm.data == obj.id)
            fluxes = [np.nansum(self.all_data[band][obj_segm]) for band in self.band_list]
            if abs(max(fluxes) / min(fluxes)) > 16:
                # No real galaxy has g-i colour exeeding 3
                # print("Skipping artefact")
                continue
            x_cen = obj.xcentroid.value
            y_cen = obj.ycentroid.value
            crop_size = min(object_size, x_cen, self.x_image_size-x_cen,
                            y_cen, self.y_image_size-y_cen)
            x_min = int(x_cen)-int(crop_size)
            x_max = int(x_cen)+int(crop_size)
            y_min = int(y_cen)-int(crop_size)
            y_max = int(y_cen)+int(crop_size)
            obj_data_list = []
            obj_header_list = []
            for band in self.band_list:
                ra, dec = self.all_wcs[band].all_pix2world([x_cen], [y_cen], 0)
                cropped_data = self.all_data[band][y_min:y_max, x_min:x_max]
                if np.max(cropped_data) == 0:
                    # Sometimes data is missing in one band or another. We need to skip those images
                    continue
                cropped_header = {}
                # Save some valuable properties into the fits file header
                cropped_header["RA_CEN"] = ra[0]
                cropped_header["DEC_CEN"] = dec[0]
                cropped_header["ELLIP"] = obj.ellipticity.value
                cropped_header["POSANG"] = obj.orientation.value
                cropped_header["FLUX"] = obj.source_sum
                cropped_header["SMA"] = obj.semimajor_axis_sigma.value
                obj_data_list.append(cropped_data)
                obj_header_list.append(cropped_header)
            cropped_objects.append(MultibandImage(obj_data_list, obj_header_list, self.band_list))
        return cropped_objects


class MultibandImage(object):
    """
    A class that represents a galaxy image taken in several pass bands
    """
    def __init__(self, data_list, header_list, band_list):
        """
        band_list -- a list of passbands
        path_list -- a paths to the images, one per passband
        """
        self.band_list = band_list
        self.ref_band = band_list[0]
        self.object_params = None
        self.back_noise = {}
        # Load data
        self.all_data = {}
        self.all_headers = {}
        self.all_data_orig = {}
        for band, data, header in zip(self.band_list, data_list, header_list):
            self.all_headers[band] = header.copy()
            self.all_data[band] = data.copy()
            # Replace all nan values with zeros
            self.all_data[band][np.where(np.isnan(data))] = 0.0
            self.all_data_orig[band] = self.all_data[band].copy()
        self.ra_cen = float(self.all_headers[self.band_list[0]]["RA_CEN"])
        self.dec_cen = float(self.all_headers[self.band_list[0]]["DEC_CEN"])
        self.sma = float(self.all_headers[self.band_list[0]]["SMA"])
        self.combined = np.mean([d for d in self.all_data.values()], axis=0)
        self.combined = gaussian_filter(self.combined, sigma=2)


    def get_obj_params(self):
        """
        Finds parameters of the main object on the stack using photutils and the first band in list
        """
        # Detect sources
        threshold = detect_threshold(self.combined, nsigma=3.5)
        segm = detect_sources(self.combined, threshold, npixels=15)
        if segm is None:
            self.object_params = None
            self.segm_data = np.zeros_like(self.all_data[self.ref_band])
            return False
        self.segm_data = segm.data
        cat = source_properties(self.all_data[self.ref_band], segm)
        # Find the central object
        y_size, x_size = self.all_data[self.ref_band].shape
        x_cen = x_size / 2
        y_cen = y_size / 2
        min_dist = 1e10
        obj_idx = self.segm_data[int(y_cen), int(x_cen)]
        if obj_idx == 0:
            # No object at the center, find the nearest one
            for obj in cat:
                if obj.area.value < 50:
                    continue
                dist = np.hypot(x_cen-obj.xcentroid.value, y_cen-obj.ycentroid.value)
                if dist < min_dist:
                    self.object_params = obj
                    min_dist = dist
        else:
            self.object_params = cat[obj_idx-1]
        if self.object_params is None:
            return False
        # Compute background noise for the future use
        for band in self.band_list:
            back_inds = np.where((self.segm_data == 0) * (self.all_data[band] != 0))
            self.back_noise[band] = np.random.choice(self.all_data[band][back_inds], size=(y_size, x_size))
        return True

    def zoom_all_data(self, target_size):
        """
        Function zoomes all the data to the given target size
        """
        initial_size = self.all_data[self.ref_band].shape[0]
        scale = target_size / initial_size
        for band in self.band_list:
            self.all_data[band] = zoom(input=self.all_data[band], zoom=scale)
        if 0.75*initial_size > target_size:
            return True
        else:
            return False

    def norm_all_data(self):
        """
        Function normalizes fluxes in all the bands
        """
        min_value = min([np.min(self.all_data[band]) for band in self.band_list])
        max_value = max([np.max(self.all_data[band]) for band in self.band_list])
        for band in self.band_list:
            self.all_data[band] = (self.all_data[band] - min_value) / (max_value - min_value)

    def as_array(self):
        """
        Function returns all the data as a 3d array
        """
        return np.array([self.all_data[band] for band in self.band_list]).copy()

    def save(self, out_name):
        """
        Save data as a multi-layer fits file
        """
        fits.PrimaryHDU(data=self.as_array()).writeto(out_name, overwrite=True)

    def unpack(self, path_to_save, common_name, orig=True, bands=None):
        """
        Save data as a separate images to a given path. The images will have names
        common_name_{band1}.fits, common_name_{band2}.fits etc.
        If orig is true: save original (unprocessed) data, else: save processed (zoomed, normed) data
        """
        if bands is None:
            band_list = self.band_list
        else:
            band_list = bands
        for band in band_list:
            out_name = path.join(path_to_save, f"{common_name}_{band}.fits")
            if orig is True:
                data = self.all_data_orig[band]
            else:
                data = self.all_data[band]
            header = fits.Header(self.all_headers[band])
            fits.PrimaryHDU(data=data, header=header).writeto(out_name, overwrite=True)
