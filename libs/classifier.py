#! /usr/bin/env python

import glob
import warnings
from os import path
import numpy as np
import keras


warnings.filterwarnings("ignore", "tensorflow")


class Classifier(object):
    def __init__(self, path_to_models):
        # Load an ansamble of models
        self.models = []
        print(f"Loading models from {path_to_models}")
        for model_file in sorted(glob.glob(path.join(path_to_models, "*.h5"))):
            self.models.append(keras.models.load_model(model_file))
        self.n_of_models = len(self.models)
        print(f"{self.n_of_models} loaded")

    def predict(self, data, fraction=0.5):
        """
        The net prediction is considered as positive if the fraction of positive
        predictions is higher than the given value
        """
        n_objects = data.shape[0]
        votes = np.empty((len(self.models), n_objects))
        for idx, model in enumerate(self.models):
            votes[idx, :] = model.predict(data)[:, 0]
        votes_rounded = np.zeros_like(votes)
        votes_rounded[votes > 0.5] = 1
        votes_rounded = np.sum(votes_rounded, axis=0)
        predictions = []
        for idx in range(n_objects):
            votes_str = "|".join([f"{p:1.3f}" for p in votes[:, idx]])
            if votes_rounded[idx] / self.n_of_models > fraction:
                decision = True
            else:
                decision = False
            predictions.append({"votes": votes_str, "decision": decision})
        return predictions
