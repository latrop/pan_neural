#!/usr/bin/env python

import os
from os import path
import tempfile
import datetime
import time
import requests
import multiprocessing
import logging
import paramiko
import numpy as np
from astropy.io import fits

def connected_to_internet(url='http://www.google.com/', timeout=5):
    try:
        _ = requests.get(url, timeout=timeout)
        return True
    except requests.ConnectionError:
        print("No internet connection available.")
    return False


class Tile(object):
    """
    Object represents a Pan-STARRS field tile
    """
    def __init__(self, cell_number, sky_cell_location):
        self.cell_number = cell_number
        self.sky_cell_location = sky_cell_location

    def __repr__(self):
        return f"Tile(cell_number={self.cell_number}, sky_cell_location={self.sky_cell_location})"

    def __str__(self):
        return f"Tile_{self.cell_number:04}_{self.sky_cell_location:03}"

    def get_url(self, band):
        url = "http://ps1images.stsci.edu/rings.v3.skycell/"
        url += f"{self.cell_number:04}/{self.sky_cell_location:03}/"
        url += f"rings.v3.skycell.{self.cell_number:04}.{self.sky_cell_location:03}.stk.{band}.unconv.fits"
        return url

    def check_existence(self, bands):
        for band in bands:
            attempt_num = 0
            while 1:
                attempt_num += 1
                try:
                    status = requests.head(self.get_url(band)).status_code
                    break
                except requests.exceptions.ConnectionError:
                    print("Conn error while checking file existance.")
                    if attempt_num <= 5:
                        print("Trying again.")
                    else:
                        print("Giving up.")
                        status = 404
                        break
            if status == 404:
                print(f"Does not exist in {band} band")
                logging.info(f"Tile {self.__str__()} does not exist in {band} band")
                return False
        return True

    def download(self, band):
        """
        Download a tile to a local file. If downloading has failed,
        it will restart, up to 5 attempts.
        Returns (OK, HDU) if downloading successful,
        or (reason of fail, None) if not.
        """
        url = self.get_url(band)
        attempt_num = 0
        while 1:
            attempt_num += 1
            try:
                req = requests.get(url)
            except requests.exceptions.ConnectionError:
                print("Connection error.")
                if attempt_num < 5:
                    print("Trying again.")
                    continue
                else:
                    print("Giving up after 5 attempts.")
                    return (f"Connection error (band {band})", None)
            except:  # Really bad practice
                print("Unexpected connection error.")
                if attempt_num < 5:
                    print("Trying again.")
                    continue
                else:
                    print("Giving up after 5 attempts.")
                    return (f"Connection error (band {band})", None)

            if req.status_code != 200:
                print(f"Status code {req.status_code} != 200")
                if attempt_num < 5:
                    print("Trying again")
                    continue
                else:
                    print("Giving up after 5 attempts.")
                    return (f"status code = req.status_code (band {band})", None)

            if int(req.headers["Content-Length"]) != len(req.content):
                print("Content length from header does not match acquired data.")
                if attempt_num < 5:
                    print("Trying again")
                    continue
                else:
                    print("Giving up after 5 attempts.")
                    return (f"Incomplete file (band {band})", None)
            # If we've got this far, than no (expected) errors have happen. We can
            # break from the dowloading cycle and proceed to file writing
            break

        # Use tempfile object to convert downloaded binary data into fits HDU
        # without saving it as a fits file
        tf = tempfile.TemporaryFile()
        tf.write(req.content)
        hdu = fits.open(tf, mode="update")
        return ("OK", hdu)

    def download_parallel(self, bands):
        """
        Download several bands in a parallel mode
        """
        print(f"Downloading tile in {bands} bands...")

        # At first let's check if there internet connection at all
        while 1:
            if connected_to_internet():
                break
            else:
                print("No internet connection. Waiting 10 secs before trying again.")
                time.sleep(10)

        # Check if all files exist on server
        if self.check_existence(bands) is False:
            return None
        pool = multiprocessing.pool.ThreadPool(len(bands))
        # Start downloading
        t_start = time.time()
        download_results = pool.starmap(self.download, zip(bands))
        pool.close()
        pool.join()
        # Check if all files dowloaded successfully
        statuses = [res[0] for res in download_results]
        if statuses.count("OK") == len(bands):
            t_elapsed = time.time() - t_start
            print(f"All files fetched successfully in {t_elapsed: 1.1f} seconds.")
            return [res[1] for res in download_results]
        else:
            print("An error occured during downloading")
            logging.info(f"Problem with tile {self} downloading. Status codes: {statuses}")
            return None


class ChunkRemote(object):
    """
    A chunk of tiles to process. This class is used to spread the job between the machines and
    to allow one to continue the interrupted jobs
    """
    def __init__(self, todo_tiles, done_tiles=None, failed_tiles=None, store_path=".", pwd=None):
        self.todo_tiles = todo_tiles
        self.tiles_in_chunk = len(todo_tiles)
        if done_tiles is None:
            self.done_tiles = []
        else:
            self.done_tiles = done_tiles
        if failed_tiles is None:
            self.failed_tiles = []
        else:
            self.failed_tiles = failed_tiles
        self.store_path = store_path
        self.pwd = pwd

    @classmethod
    def get_initial_chunk(cls, pwd):
        todo_tiles = []
        return cls(todo_tiles, pwd=pwd)

    @classmethod
    def load_chunk(cls, directory, pwd):
        """
        Load a chunk from a local directory, that contins todo.dat, done.dat and failed.dat files
        """

        # Load todo tiles
        todo_tiles = []
        for line in open(path.join(directory, "todo.dat")):
            if line.startswith("#"):
                continue
            cell_number = int(line.split()[0])
            sky_cell_location = int(line.split()[1])
            todo_tiles.append(Tile(cell_number, sky_cell_location))

        # Load finished tiles
        done_tiles = []
        if path.exists(path.join(directory, "done.dat")):
            for line in open(path.join(directory, "done.dat")):
                if line.startswith("#"):
                    continue
                cell_number = int(line.split()[0])
                sky_cell_location = int(line.split()[1])
                done_tiles.append(Tile(cell_number, sky_cell_location))

        # Load failed tiles
        failed_tiles = []
        if path.exists(path.join(directory, "failed.dat")):
            for line in open(path.join(directory, "failed.dat")):
                if line.startswith("#"):
                    continue
                cell_number = int(line.split()[0])
                sky_cell_location = int(line.split()[1])
                failed_tiles.append(Tile(cell_number, sky_cell_location))

        print(f"      {len(done_tiles)} are done;")
        print(f"      {len(failed_tiles)} are failed.")
        logging.info(f"{len(todo_tiles)} tiles loaded locally.")
        return cls(todo_tiles, done_tiles, failed_tiles, directory, pwd=pwd)

    def store_chunk(self, directory=None):
        """
        Store all the tiles into a file
        """
        if directory is None:
            directory = self.store_path
        # Store unfinished tiles
        with open(path.join(directory, "todo.dat"), "w") as fout:
            fout.write("# Saved: ")
            fout.write(datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S'))
            fout.write(f"\n# Number of tiles: {len(self.todo_tiles)}\n")
            fout.write("# Tiles:\n")
            for tile in self.todo_tiles:
                fout.write(f"{tile.cell_number} {tile.sky_cell_location}\n")

        # Store finished tiles
        with open(path.join(directory, "done.dat"), "w") as fout:
            fout.write("# Saved: ")
            fout.write(datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S'))
            fout.write(f"\n# Number of tiles: {len(self.done_tiles)}\n")
            fout.write("# Tiles:\n")
            for tile in self.done_tiles:
                fout.write(f"{tile.cell_number} {tile.sky_cell_location}\n")

        # Store failed tiles
        with open(path.join(directory, "failed.dat"), "w") as fout:
            fout.write("# Saved: ")
            fout.write(datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S'))
            fout.write(f"\n# Number of tiles: {len(self.failed_tiles)}\n")
            fout.write("# Tiles:\n")
            for tile in self.failed_tiles:
                fout.write(f"{tile.cell_number} {tile.sky_cell_location}\n")
        # print(f"Chunk list is saved.")
        self.store_path = directory
        logging.info("Chunk list is saved.")

    def get_working_tile(self):
        """
        Returns a copy of a new tile to be processed
        """
        if len(self.todo_tiles) > 0:
            return self.todo_tiles[0]
        else:
            self.get_new_portion()
        self.store_chunk()
        return self.todo_tiles[0]

    def get_new_portion(self):
        """
        Go to a remote server to get new portion of tiles
        """
        logging.info("Going to remote server for a new chunk")
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname='observ.astro.spbu.ru', username='panneur', password=self.pwd)

        while 1:
            stdin, stdout, stderr = ssh.exec_command(f'./dispenser_dr2.py {os.uname().nodename}')
            for butes in stdout.read().splitlines():
                line = butes.decode("utf-8")
                if "Empty" in line:
                    logging.info("No more tiles on the remote server")
                    exit()
                if "Busy" in line:
                    logging.info("Server is busy. Waiting for 5 seconds.")
                    time.sleep(5)
                    break
                cell_number = int(line.split()[0])
                sky_cell_location = int(line.split()[1])
                self.todo_tiles.append(Tile(cell_number, sky_cell_location))
            else:
                logging.info(f"Got {len(self.todo_tiles)} from remote server")
                break
        ssh.close()

    def mark_working_tile_as_done(self, save=False):
        """
        Move the current working tile from the todo list the list of done tiles.
        If save is True => save the chunk
        """
        self.done_tiles.append(self.todo_tiles.pop(0))
        logging.info(f"{self.done_tiles[-1]} is done.")
        if save is True:
            self.store_chunk()

    def mark_working_tile_as_failed(self, save=False):
        """
        Move the current working tile from the todo list the list of failed tiles.
        If save is True => save the chunk
        """
        self.failed_tiles.append(self.todo_tiles.pop(0))
        logging.info(f"Tile {self.failed_tiles[-1]} failed.")
        if save is True:
            self.store_chunk()
