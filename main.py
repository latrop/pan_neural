#!/usr/bin/env python

import sys
import os
from os import path
import logging
import time
import numpy as np
from libs.tiles import ChunkRemote as Chunk
from libs.MultibandImage import MultibandField
from libs.classifier import Classifier


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def process_tile(tile, classifier_list):
    """
    Perform full analysis of a tile: downoading the field images,
    segmenting them, detecting objects.
    """
    times = {}
    # Download field images
    start_time = time.time()
    hdus = tile.download_parallel(bands=["g", "r", "i"])
    times["download"] = time.time() - start_time
    if hdus is None:
        # A problem occured during the downloading
        return [None, None], None

    # Load field images as a MultibandField object
    field = MultibandField(hdu_list=hdus, band_list=["g", "r", "i"])

    # Perform the image segmentation and crop out objects
    start_time = time.time()
    print("Searching objects on the tile...")
    object_images = field.crop_out_objects(minsize=48)
    if (object_images is None) or (len(object_images) == 0):
        print("No large objects found on the tile.")
        return [None, None], None
    print(f"Found {len(object_images)} on the tile. Checking them all with the ANN models.")
    times["segmentation"] = time.time() - start_time

    # Check every object using model
    start_time = time.time()
    data_to_predict = []
    for obj in object_images:
        obj.zoom_all_data(target_size=48)
        obj.norm_all_data()
        data_to_predict.append(np.dstack(obj.as_array()))

    if len(data_to_predict) == 0:
        return [None, None], None

    data_to_predict = np.array(data_to_predict)

    candidates = []
    for classifier in classifier_list:
        candidates.append([])
        predictions = classifier.predict(data_to_predict)
        for idx, obj in enumerate(object_images):
            if predictions[idx]["decision"] is True:
                candidates[-1].append([obj, predictions[idx]["votes"]])
    times["ANN"] = time.time() - start_time
    return candidates, times


def main(pwd):
    res_dir = path.join(path.dirname(path.realpath(__file__)), "results")
    candidates_dir = path.join(res_dir, "candidates")

    # Set up logging
    try:
        logging.basicConfig(filename="log.log", level=logging.INFO,
                            format='%(asctime)s %(message)s',
                            datefmt='%d.%m.%Y %H:%M:%S | ', force=True)
    except ValueError:
        # For older versiong of logging
        logging.basicConfig(filename="log.log", level=logging.INFO,
                            format='%(asctime)s %(message)s',
                            datefmt='%d.%m.%Y %H:%M:%S | ')

    # Load classifiers
    dirname = path.dirname(__file__)
    classifier_old = Classifier(path.join(dirname, 'models_dr1'))
    classifier = Classifier(path.join(dirname, 'models_dr2'))

    if path.exists(res_dir):
        # If res_dir path exists, it means that we are continuing from the previously saved state.
        logging.info("Resuming from the previously saved state")
        chunk = Chunk.load_chunk(res_dir, pwd)
    else:
        # If res_dir does not exist, it means that we are starting from scratch. We need to create
        # both res_dir and a new chunk
        logging.info("Program started from scratch")
        os.makedirs(res_dir)
        chunk = Chunk.get_initial_chunk(pwd)
        chunk.store_chunk(res_dir)

    candidates_file_path = path.join(res_dir, "all_candidates.dat")
    if not path.exists(candidates_file_path):
        candidates_file = open(candidates_file_path, "w")
        candidates_file.write("# Tile            id        ra              dec     size[pix] ")
        candidates_file.write("votes\n")
        candidates_file.close()

    # num_of_tiles_todo = len(chunk.todo_tiles)
    # script_start_time = time.time()
    idx = 0
    while 1:
        idx += 1
        tile = chunk.get_working_tile()
        logging.info(f"Starting tile {tile}.")
        print(f"\n\nStarting tile {tile}")
        tile_start_time = time.time()
        res, processing_time = process_tile(tile, (classifier, classifier_old))
        candidates = res[0]
        candidates_old = res[1]
        # Merge results of two neural networks
        if (candidates is None) and (candidates_old is None):
            candidates_both = None
        elif (candidates is None) and (candidates_old is not None):
            candidates_both = candidates_old[:]
        elif (candidates is not None) and (candidates_old is None):
            candidates_both = candidates[:]
        else:
            candidates_both = candidates[:]
            for obj_old in candidates_old:
                for i in range(len(candidates)):
                    obj = candidates_both[i]
                    dist = np.hypot(obj_old[0].ra_cen-obj[0].ra_cen, obj_old[0].dec_cen-obj[0].dec_cen)
                    if dist < 0.0001:
                        # This is the same object, add the votes for the second model
                        candidates_both[i][1] += f";{obj_old[1]}"
                        break
                else:
                    # This object was not detected by the other network
                    candidates_both.append(obj_old)

        tile_end_time = time.time()

        # Save new candidates
        if candidates_both is None:
            # Something went wrong during the tile processing. Let's mark the tile as failed
            chunk.mark_working_tile_as_failed(save=True)
        else:
            print(f" -> Found {len(candidates_both)} candidates")
            # Tile processing finished. Save the candidates
            msg = f"{tile} processed in {tile_end_time-tile_start_time:.1f} seconds; "
            msg += f"{len(candidates_both)} candidate(s) found"
            logging.info(msg)
            if len(candidates_both) > 0:
                tile_dir = path.join(candidates_dir, str(tile))
                if not path.exists(tile_dir):
                    os.makedirs(tile_dir)
                candidates_file = open(candidates_file_path, "a")
                for i in range(len(candidates_both)):
                    obj, votes = candidates_both[i]
                    # Save candidate fits images
                    obj.unpack(tile_dir, f"candidate_{i}", bands=["r"])
                    # Save info in a common file
                    candidates_file.write(f"{str(tile)}   {i}    ")
                    candidates_file.write(f"{obj.ra_cen:.4f}   {obj.dec_cen:.4f}   {obj.sma:.4f}   ")
                    candidates_file.write(f"{votes}\n")
                candidates_file.close()
            chunk.mark_working_tile_as_done(save=True)
            print(f"Tile {tile} processing completed in {tile_end_time-tile_start_time:.1f} sec")
            print(f"(downloading: {processing_time['download']:1.1f} / segmentation: {processing_time['segmentation']:1.1f} / ANN: {processing_time['ANN']:1.1f})")


if __name__ == '__main__':
    main(sys.argv[1])
