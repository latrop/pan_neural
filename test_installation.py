#!/usr/bin/env python

import os
import sys

major = sys.version_info[0]
minor = sys.version_info[1]
if (major < 3) or (minor < 6):
    print("Python version (%i.%i) is too old. At least 3.6 is required."% (major, minor))
else:
    print("Python version: OK")

try:
    import requests
    print("Requests: OK")
except ImportError:
    print("Requests: failure")

try:
    import numpy
    print("Numpy: OK")
except ImportError:
    print("Numpy: failure")

try:
    import scipy
    print("Scipy: OK")
except ImportError:
    print("Scipy: failure")

try:
    import astropy
    print("Astropy: OK")
except ImportError:
    print("Astropy: failure")

try:
    import photutils
    print("Photutils: OK")
except ImportError:
    print("Photutils: failure")

try:
    import tensorflow
    print("Tensorflow: OK")
except ImportError:
    print("Tensorflow: failure")

try:
    stderr = sys.stderr
    sys.stderr = open(os.devnull, 'w')
    import keras
    sys.stderr = stderr
    print("Keras: OK")
except ImportError:
    print("Keras: failure")

try:
    import paramiko
    print("paramiko: OK")
except ImportError:
    print("paramiko: failure")
